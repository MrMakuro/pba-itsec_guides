https://www.tecmint.com/add-new-disk-to-an-existing-linux/
# New disk
## Partition
fdisk -l

fdisk /dev/xxxx

- n – Create partition
- p – print partition table
- d – delete a partition
- q – exit without saving the changes
- w – write the changes and exit.

n
p
1
2048
{size of the disk}

## Filsystem
mkfs.ext4 /dev/xxxx1

## Mount
mount /dev/xxxx1 /{place you want to mount}

## Automatisk mount
vi /etc/fstab
/dev/xxxx1	/{place you want to mount}	ext4	defaults     0   0