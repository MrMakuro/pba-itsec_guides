# Install pip and Suricata (If not installed already)
sudo apt install python-pip suricata

# Install Suricata-update
pip install --upgrade suricata-update

# Add suricata-update to PATH
export PATH=$HOME/.local/bin:$PATH

# Add suricata group
```bash
sudo groupadd suricata
sudo mkdir -p /var/lib/suricata/rules
sudo mkdir -p /var/lib/suricata/update

sudo chgrp -R suricata /etc/suricata
sudo chgrp -R suricata /var/lib/suricata/rules
sudo chgrp -R suricata /var/lib/suricata/update

sudo chmod -R g+r /etc/suricata/
sudo chmod -R g+rw /var/lib/suricata/rules
sudo chmod -R g+rw /var/lib/suricata/update

# sudo usermod -a -G suricata <username>
sudo usermod -a -G suricata foo

ls -al /etc/suricata
ls -al /var/lib/suricata/rules
ls -al /var/lib/suricata/update
```

# Download suricata rules
wget https://rules.emergingthreats.net/open/suricata-3.0/emerging.rules.tar.gz

sudo tar xzf emerging.rules.tar.gz

# Backup rules folder
cp -r /etc/suricata/rules /etc/suricata/rules.bak

# Add emergingthreats rules
cp -r ./rules/* /etc/suricata/rules/

# Run
```bash
# sudo suricata -r <pcap file>
sudo suricata -r eternalromance-success-2008r2.pcap
```

# Usefull Curicata links
- https://suricata-ids.org/download/
- https://rules.emergingthreats.net/OPEN_download_instructions.html
- https://rules.emergingthreats.net/open/suricata-3.0/