# Impala
```bash
# remove cdrom from sources list
sudo vi /etc/apt/sources.list
# following line could work in a script
# sed -i '/^[^#]/ s/\(^.*cdrom.*$\)/#\1/' /etc/apt/sources.list


sudo apt update; sudo apt upgrade

sudo apt install build-essential git texinfo bison autoconf automake libtool libz-dev libssl-dev libncurses-dev libsasl2-dev libkrb5-dev python2.7

sudo update-alternatives --install /usr/bin/python python /usr/bin/python2.7 10

git clone https://gitbox.apache.org/repos/asf/impala.git ~/Impala

# Make sure the user have rights to the Impala directory and the directory above it as it will build an extended directory structure.
sudo chown -R foo /home/foo/Impala # replace with your impala path and correct user

sudo chmod 750 /home/foo/ Impala # replace with your impala path and correct user

sudo vi ~/.bashrc
export IMPALA_HOME=/home/foo/Impala # replace with your impala path
export BOOST_LIBRARYDIR=/usr/lib/x86_64-linux-gnu

$IMPALA_HOME/bin/bootstrap_system.sh

. ~/.bashrc

source $IMPALA_HOME/bin/impala-config.sh

# ONCE
# consider running -release
${IMPALA_HOME}/buildall.sh -noclean -notests -format

# Might want to restart to loosen up the RAM

source $IMPALA_HOME/bin/impala-config.sh

$IMPALA_HOME/bin/create-test-configuration.sh -create_metastore -create_sentry_policy_db

$IMPALA_HOME/testdata/bin/run-all.sh

${IMPALA_HOME}/bin/start-impala-cluster.py
```

# turn off ipv6
## Ubuntu 18.04
```bash
sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1
sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1
```
## ubuntu 16.04
```bash
# create the long-life config file
echo "net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1" | sudo tee /etc/sysctl.d/99-my-disable-ipv6.conf

# ask the system to use it
sudo service procps reload

# check the result
cat /proc/sys/net/ipv6/conf/all/disable_ipv6
```


# Impala usefull links
- https://docs.cloudera.com/runtime/7.0.3/impala-reference/topics/impala-ports.html
- https://docs.cloudera.com/documentation/enterprise/5-8-x/topics/impala_connecting.html
- https://cwiki.apache.org/confluence/display/IMPALA/Impala+Build+Prerequisites
- https://cwiki.apache.org/confluence/display/IMPALA/Building+native-toolchain+from+scratch+and+using+with+Impala
- https://cwiki.apache.org/confluence/display/IMPALA/Building+Impala