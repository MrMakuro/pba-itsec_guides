#!/usr/bin/env bash

echo kill old java instances
kill $(ps aux | grep java | cut -c 10-17)

echo kill old catalogds
kill $(ps aux | grep catalogd | cut -c 10-17)

echo kill old statestored instances
kill $(ps aux | grep statestored | cut -c 10-17)

echo kill old impalad instances
kill $(ps aux | grep impalad | cut -c 10-17)

echo kill old kudu-master instances
kill $(ps aux | grep kudu-master | cut -c 10-17)

echo kill old kudu-tserver instances
kill $(ps aux | grep kudu-tserver | cut -c 10-17)

sleep 2
echo status
ss -antl