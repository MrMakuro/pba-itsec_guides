sudo apt install python2.7 postgresql openjdk-8-jdk openjdk-8-source

update-alternatives --install /usr/bin/python python /usr/bin/python2.7 10

if [ ! -d /opt/impala/apache-maven-3.5.4 ]; then
  sudo wget -nv \
    https://downloads.apache.org/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz
  sha512sum -c - <<< '2a803f578f341e164f6753e410413d16ab60fabe31dc491d1fe35c984a5cce696bc71f57757d4538fe7738be04065a216f3ebad4ef7e0ce1bb4c51bc36d6be86  apache-maven-3.5.4-bin.tar.gz'
  sudo tar -C /opt/impala -xzf apache-maven-3.5.4-bin.tar.gz
  sudo ln -s /opt/impala/apache-maven-3.5.4/bin/mvn /usr/local/bin
fi