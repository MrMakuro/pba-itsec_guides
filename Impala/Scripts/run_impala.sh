#!/usr/bin/env bash

echo kill old java instances
kill $(ps aux | grep java | cut -c 10-17)

echo kill old catalogds
kill $(ps aux | grep catalogd | cut -c 10-17)

echo kill old statestored instances
kill $(ps aux | grep statestored | cut -c 10-17)

echo kill old impalad instances
kill $(ps aux | grep impalad | cut -c 10-17)

echo kill old kudu-master instances
kill $(ps aux | grep kudu-master | cut -c 10-17)

echo kill old kudu-tserver instances
kill $(ps aux | grep kudu-tserver | cut -c 10-17)

export IMPALA_HOME=/opt/impala/impala

source $IMPALA_HOME/bin/impala-config.sh

echo start dfs mini cluster
$IMPALA_HOME/testdata/bin/run-mini-dfs.sh

echo start hive and metastore
$IMPALA_HOME/testdata/bin/run-hive-server.sh

echo start the impala cluster
${IMPALA_HOME}/bin/start-impala-cluster.py

echo this is a sanity check
impala-shell.sh -q 'select version();'
