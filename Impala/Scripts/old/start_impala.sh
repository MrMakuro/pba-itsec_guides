#!/usr/bin/env bash

echo kill old java instances
kill $(ps aux | grep java | cut -c 10-17)

export IMPALA_HOME=/opt/impala/impala

${IMPALA_HOME}/buildall.sh -noclean -notests -start_minicluster -start_impala_cluster
source $IMPALA_HOME/bin/impala-config.sh
${IMPALA_HOME}/bin/start-impala-cluster.py

echo this is a sanity check
impala-shell.sh -q 'select version();'
