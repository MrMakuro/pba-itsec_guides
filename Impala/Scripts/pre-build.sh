#!/usr/bin/env bash
apt update
apt upgrade

sed -i '/^[^#]/ s/\(^.*cdrom.*$\)/#\1/' /etc/apt/sources.list

apt install build-essential git texinfo bison autoconf automake libtool libz-dev libssl-dev libncurses-dev libsasl2-dev libkrb5-dev python2.7

update-alternatives --install /usr/bin/python python /usr/bin/python2.7 10

git clone https://gitbox.apache.org/repos/asf/impala.git ~/Impala
