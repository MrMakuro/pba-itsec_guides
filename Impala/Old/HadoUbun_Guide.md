update && upgrade


wget http://mirrors.rackhosting.com/apache/impala/3.3.0/apache-impala-3.3.0.tar.gz

sudo tar xzf apache-impala-3.3.0.tar.gz

mv apache-impala-3.3.0.tar.gz impala

vi ~/.bashrc
- abend:
export IMPALA_HOME=/home/foo/impala # replace with your impala path
export BOOST_LIBRARYDIR=/usr/lib/x86_64-linux-gnu

sudo chown -R foo /home/foo/impala # replace with your impala path and correct user

sudo chmod 750 /home/foo/impala # replace with your impala path and correct user

### snapshot downloaded

sudo apt install python2.7

sudo update-alternatives --install /usr/bin/python python /usr/bin/python2.7 10

-------------------------
## reason
```
# General build requirements
sudo apt-get install build-essential git
sudo apt-get install texinfo
 
# Requirements for specific packages
sudo apt-get install bison # For binutils
sudo apt-get install autoconf automake libtool # For libevent
sudo apt-get install libz-dev # For OpenSSL
sudo apt-get install libssl-dev # For Thrift
sudo apt-get install libncurses-dev # For GDB
sudo apt-get install libsasl2-dev libkrb5-dev # For Kudu
```
## one-liner
sudo apt-get install build-essential git texinfo bison autoconf automake libtool libz-dev libssl-dev libncurses-dev libsasl2-dev libkrb5-dev  

vi /etc/apt/sources.list

sudo $IMPALA_HOME/bin/bootstrap_system.sh

error:
 cdrom://Ubuntu-Server 16.04.6 LTS _Xenial Xerus_ - Release amd64 (20190226)/dists/xenial/main/binary-amd64/Packages  Please use apt-cdrom to make this CD-ROM recognized by APT. apt-get update cannot be used to add new CD-ROMs
then you forgot this step:
vi /etc/apt/source.list
outcomment cdrom



source $IMPALA_HOME/bin/impala-config.sh
$IMPALA_HOME/buildall.sh -noclean -notests # ONCE

## might not work
$IMPALA_HOME/bin/create-test-configuration.sh -create_metastore -create_sentry_policy_db
$IMPALA_HOME/testdata/bin/run-all.sh
$IMPALA_HOME/bin/start-impala-cluster.py

## did work

Start supporting services.

${IMPALA_HOME}/buildall.sh -noclean -notests -start_minicluster

Start the Impala cluster.

source ${IMPALA_HOME}/bin/impala-config.sh # You must source this in your shell before most of the below commands will work.
 
${IMPALA_HOME}/bin/start-impala-cluster.py

Check that everything works correctly.

impala-shell.sh -q "SELECT version()"
Starting Impala Shell without Kerberos authentication
Connected to localhost:21000
Server version: impalad version 2.2.0-INTERNAL DEBUG (build 47c90e004aecb928a37b926080098d30b96b4330)
Query: select version()
+---------------------------------------------------------------------------------------+
| version()                                                                             |
+---------------------------------------------------------------------------------------+
| impalad version 2.2.0-INTERNAL DEBUG (build 47c90e004aecb928a37b926080098d30b96b4330) |
| Built on Sun, Mar 22 15:22:57 PDT 2015                                                |
+---------------------------------------------------------------------------------------+
Fetched 1 row(s) in 0.05s

## not tested.

# Rebuild both backend and frontend
${IMPALA_HOME}/buildall.sh -skiptests -noclean
 
source ${IMPALA_HOME}/bin/impala-config.sh # If you didn't already source impala-config.sh in this shell
 # Optional: Rebuild the impala binary only
make -j$IMPALA_BUILD_THREADS impalad
# Optional: Build the Java-side frontend only
make -j$IMPALA_BUILD_THREADS fe
# Restart the Impala cluster
${IMPALA_HOME}/bin/start-impala-cluster.py
-------------------------
