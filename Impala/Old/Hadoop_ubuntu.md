# DEPRICATED
# Hadoop_ubuntu
## install
### Java
- https://thishosting.rocks/install-java-ubuntu/

sudo apt install software-properties-common

sudo add-apt-repository ppa:linuxuprising/java

sudo apt update

sudo apt-get install oracle-java13-installer

sudo apt install oracle-java13-set-default

### hadoop
- https://www.guru99.com/how-to-install-hadoop.html#2

sudo addgroup hadoop_

sudo adduser --ingroup hadoop_ hduser_
-  set password
-  fill rest of info as wanted

sudo adduser hduser_ sudo

su hduser_

#### configure ssh

su - hduser_

ssh-keygen -t rsa -P ""
- press enter


cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys

ssh localhost

#### download hadoop

wget https://archive.apache.org/dist/hadoop/common/hadoop-3.1.3/hadoop-3.1.3.tar.gz

sudo tar xzf hadoop-3.1.3.tar.gz

sudo mv hadoop-2.2.0 hadoop

sudo chown -R hduser_:hadoop_ hadoop

#### configure hadoop
##### ~/.bashrc
(could be abended)
vi ~/.bashrc

#Set HADOOP_HOME
export HADOOP_HOME=/home/hduser_/hadoop
#Set JAVA_HOME
export JAVA_HOME=/usr/lib/jvm/java-13-oracle
# Add bin/ directory of Hadoop to PATH
export PATH=$PATH:$HADOOP_HOME/bin

. ~/.bashrc

#####  configure HDFS
(could be abended)
vi $HADOOP_HOME/etc/hadoop/hadoop-env.sh
- replace JAVA_HOME with java location
- /usr/lib/jvm/java-13-oracle

vi $HADOOP_HOME/etc/hadoop/core-site.xml
- between config/config
<property>
<name>hadoop.tmp.dir</name>
<value>/app/hadoop/tmp</value>
<description>Parent directory for other temporary directories.</description>
</property>
<property>
<name>fs.defaultFS </name>
<value>hdfs://localhost:54310</value>
<description>The name of the default file system. </description>
</property>

cd $HADOOP_HOME/etc/hadoop

sudo mkdir -p /app/hadoop/tmp

sudo chown -R hduser_:hadoop_ /app/hadoop/tmp

sudo chmod 750 /app/hadoop/tmp

##### mapped config

sudo vi /etc/profile.d/hadoop.sh

export HADOOP_HOME=/home/hduser_/hadoop

sudo chmod +x /etc/profile.d/hadoop.sh

lets do a reboot

vi $HADOOP_HOME/etc/hadoop/mapred-site.xml
- between config/config
<property>
<name>mapreduce.jobtracker.address</name>
<value>localhost:54311</value>
<description>MapReduce job tracker runs at this host and port.
</description>
</property>

vi $HADOOP_HOME/etc/hadoop/hdfs-site.xml
- between config/config
<property>
<name>dfs.replication</name>
<value>1</value>
<description>Default block replication.</description>
</property>
<property>
<name>dfs.datanode.data.dir</name>
<value>/home/hduser_/hdfs</value>
</property>

sudo mkdir -p /home/hduser_/hdfs

sudo chown -R hduser_:hadoop_ /home/hduser_/hdfs

sudo chmod 750 /home/hduser_/hdfs

##### run this

this once.
$HADOOP_HOME/bin/hdfs namenode -format

to start.
$HADOOP_HOME/sbin/start-dfs.sh

$HADOOP_HOME/sbin/start-yarn.sh

to stop.
$HADOOP_HOME/sbin/stop-dfs.sh

$HADOOP_HOME/sbin/stop-yarn.sh

jps to see running



## install
## configure
# impala
wget http://mirrors.rackhosting.com/apache/impala/3.3.0/apache-impala-3.3.0.tar.gz

sudo tar xzf apache-impala-3.3.0.tar.gz

mv apache-impala-3.3.0.tar.gz impala

vi ~/.bashrc
- abend:
export IMPALA_HOME=/home/foo/impala
export BOOST_LIBRARYDIR=/usr/lib/x86_64-linux-gnu

sudo chown -R hduser_:hadoop_ /home/hduser_/impala

sudo chmod 750 /home/hduser_/impala

sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 10

-------------------------

# General build requirements
sudo apt-get install build-essential git
sudo apt-get install texinfo
 
# Requirements for specific packages
sudo apt-get install bison # For binutils
sudo apt-get install autoconf automake libtool # For libevent
sudo apt-get install libz-dev # For OpenSSL
sudo apt-get install libssl-dev # For Thrift
sudo apt-get install libncurses-dev # For GDB
sudo apt-get install libsasl2-dev libkrb5-dev # For Kudu

sudo apt-get install build-essential git texinfo bison autoconf automake libtool libz-dev libssl-dev libncurses-dev libsasl2-dev libkrb5-dev  
sudo apt-get remove build-essential git texinfo bison autoconf automake libtool libz-dev libssl-dev libncurses-dev libsasl2-dev libkrb5-dev  

$IMPALA_HOME/bin/bootstrap_system.sh

error:
 cdrom://Ubuntu-Server 16.04.6 LTS _Xenial Xerus_ - Release amd64 (20190226)/dists/xenial/main/binary-amd64/Packages  Please use apt-cdrom to make this CD-ROM recognized by APT. apt-get update cannot be used to add new CD-ROMs
fix:
vi /etc/apt/source.list
outcomment cdrom


-------------------------

# hive

wget http://ftp.download-by.net/apache/hive/hive-3.1.2/apache-hive-3.1.2-bin.tar.gz

sudo tar xzf apache-hive-3.1.2-bin.tar.gz

mv apache-hive-3.1.2-bin hive

vi ~/.bashrc
- abend:
export CLASSPATH=$CLASSPATH:/home/hduser_/hive/lib/*:.
export CLASSPATH=$CLASSPATH:/home/hduser_/hadoop/lib/*:.

cd /home/hduser_/hive/conf/

cp hive-env.sh.template hive-env.sh

vi hive-env.sh
- abend
export HADOOP_HOME=/home/hduser_/hadoop

sudo chown -R hduser_:hadoop_ /home/hduser_/hive

sudo chmod 750 /home/hduser_/hive


# zeek (bro)

sudo apt install bro