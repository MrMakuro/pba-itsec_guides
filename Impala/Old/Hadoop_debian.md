# DEAD

https://tecadmin.net/install-oracle-java-on-debian-10-buster/

## Download and Install Hadoop
### Add a Hadoop system user using below command

```sudo addgroup hadoop_```


```sudo adduser --ingroup hadoop_ hduser_```


Enter your password, name and other details.

NOTE: There is a possibility of below-mentioned error in this setup and installation process.

"hduser is not in the sudoers file. This incident will be reported."



This error can be resolved by Login as a root user



Execute the command

```sudo adduser hduser_ sudo```


```Re-login as hduser_```


### Configure SSH

In order to manage nodes in a cluster, Hadoop requires SSH access

First, switch user, enter the following command

```su - hduser_```


This command will create a new key.

```ssh-keygen -t rsa -P ""```


Enable SSH access to local machine using this key.

```cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys```


Now test SSH setup by connecting to localhost as 'hduser' user.

```ssh localhost```


Note: Please note, if you see below error in response to 'ssh localhost', then there is a possibility that SSH is not available on this system-

```
ssh: connect to host localhost port 22: connection refused
```

To resolve this -

Purge SSH using,

```sudo apt-get purge openssh-server```
It is good practice to purge before the start of installation



Install SSH using the command-

```sudo apt-get install openssh-server```


### Next step is to Download Hadoop



Select Stable



Select the tar.gz file ( not the file with src)



Once a download is complete, navigate to the directory containing the tar file



Enter,

```sudo tar xzf hadoop-2.2.0.tar.gz```


Now, rename hadoop-2.2.0 as hadoop

```sudo mv hadoop-2.2.0 hadoop```


```sudo chown -R hduser_:hadoop_ hadoop```


## Configure Hadoop
### Modify ~/.bashrc file

Add following lines to end of file ~/.bashrc
```
#Set HADOOP_HOME
export HADOOP_HOME=<Installation Directory of Hadoop>
#Set JAVA_HOME
export JAVA_HOME=<Installation Directory of Java>
# Add bin/ directory of Hadoop to PATH
export PATH=$PATH:$HADOOP_HOME/bin
```


Now, source this environment configuration using below command

```. ~/.bashrc```


### Configurations related to HDFS

Set JAVA_HOME inside file $HADOOP_HOME/etc/hadoop/hadoop-env.sh

With

```export JAVA_HOME=/location of your jdk13.0.2```

There are two parameters in $HADOOP_HOME/etc/hadoop/core-site.xml which need to be set-

1. 'hadoop.tmp.dir' - Used to specify a directory which will be used by Hadoop to store its data files.

2. 'fs.default.name' - This specifies the default file system.

To set these parameters, open core-site.xml

```sudo gedit $HADOOP_HOME/etc/hadoop/core-site.xml```


Copy below line in between tags ```<configuration></configuration>```
```xml
<property>
<name>hadoop.tmp.dir</name>
<value>/app/hadoop/tmp</value>
<description>Parent directory for other temporary directories.</description>
</property>
<property>
<name>fs.defaultFS </name>
<value>hdfs://localhost:54310</value>
<description>The name of the default file system. </description>
</property>
```

Navigate to the directory $HADOOP_HOME/etc/Hadoop



Now, create the directory mentioned in core-site.xml

```sudo mkdir -p <Path of Directory used in above setting>```


Grant permissions to the directory

```sudo chown -R hduser_:Hadoop_ <Path of Directory created in above step>```


```sudo chmod 750 <Path of Directory created in above step>```


### Map Reduce Configuration

Before you begin with these configurations, lets set HADOOP_HOME path

```sudo gedit /etc/profile.d/hadoop.sh```

And Enter

```export HADOOP_HOME=/home/guru99/Downloads/Hadoop```


Next enter

```sudo chmod +x /etc/profile.d/hadoop.sh```


Exit the Terminal and restart again

Type ```echo $HADOOP_HOME.``` To verify the path



Now copy files
```
sudo cp $HADOOP_HOME/etc/hadoop/mapred-site.xml.template $HADOOP_HOME/etc/hadoop/mapred-site.xml
```

Open the mapred-site.xml file

```sudo gedit $HADOOP_HOME/etc/hadoop/mapred-site.xml```


Add below lines of setting in between tags <configuration> and </configuration>
```xml
<property>
<name>mapreduce.jobtracker.address</name>
<value>localhost:54311</value>
<description>MapReduce job tracker runs at this host and port.
</description>
</property>
```

Open $HADOOP_HOME/etc/hadoop/hdfs-site.xml as below,

```sudo gedit $HADOOP_HOME/etc/hadoop/hdfs-site.xml```
 

 



Add below lines of setting between tags <configuration> and </configuration>
```xml
<property>
<name>dfs.replication</name>
<value>1</value>
<description>Default block replication.</description>
</property>
<property>
<name>dfs.datanode.data.dir</name>
<value>/home/hduser_/hdfs</value>
</property>
```

Create a directory specified in above setting-
```
sudo mkdir -p <Path of Directory used in above setting>
sudo mkdir -p /home/hduser_/hdfs


sudo chown -R hduser_:hadoop_ <Path of Directory created in above step>
sudo chown -R hduser_:hadoop_ /home/hduser_/hdfs


sudo chmod 750 <Path of Directory created in above step>
sudo chmod 750 /home/hduser_/hdfs
```

### Before we start Hadoop for the first time, format HDFS using below command

```$HADOOP_HOME/bin/hdfs namenode -format```


### Start Hadoop single node cluster using below command

```$HADOOP_HOME/sbin/start-dfs.sh```
An output of above command



```$HADOOP_HOME/sbin/start-yarn.sh```


Using 'jps' tool/command, verify whether all the Hadoop related processes are running or not.



If Hadoop has started successfully then an output of jps should show NameNode, NodeManager, ResourceManager, SecondaryNameNode, DataNode.

### Stopping Hadoop

```$HADOOP_HOME/sbin/stop-dfs.sh```


```$HADOOP_HOME/sbin/stop-yarn.sh```


------
## Step 4: Installing Hive
The following steps are required for installing Hive on your system. Let us assume the Hive archive is downloaded onto the /Downloads directory.

### Extracting and verifying Hive Archive
The following command is used to verify the download and extract the hive archive:
```
$ tar zxvf apache-hive-3.1.2-bin.tar.gz
$ ls
```
On successful download, you get to see the following response:

apache-hive-3.1.2-bin apache-hive-3.1.2-bin.tar.gz
### Copying files to /home/hduser_/hive directory
We need to copy the files from the super user “su -”. The following commands are used to copy the files from the extracted directory to the /usr/local/hive” directory.
```
$ su -
passwd:


cd Download location
mv apache-hive-3.1.2-bin /home/hduser_/hive
exit
```
### Setting up environment for Hive
You can set up the Hive environment by appending the following lines to ~/.bashrc file:
```
export HIVE_HOME=/home/hduser_/hive
export PATH=$PATH:$HIVE_HOME/bin
export CLASSPATH=$CLASSPATH:/home/hduser_/Hadoop/lib/*:.
export CLASSPATH=$CLASSPATH:/home/hduser_/hive/lib/*:.
```
The following command is used to execute ~/.bashrc file.

`$ source ~/.bashrc`
## Step 5: Configuring Hive
To configure Hive with Hadoop, you need to edit the hive-env.sh file, which is placed in the $HIVE_HOME/conf directory. The following commands redirect to Hive config folder and copy the template file:

$ cd $HIVE_HOME/conf
$ cp hive-env.sh.template hive-env.sh
Edit the hive-env.sh file by appending the following line:

export HADOOP_HOME=/home/hduser_/hadoop

Hive installation is completed successfully. Now you require an external database server to configure Metastore. We use Apache Derby database.

## Impala configure