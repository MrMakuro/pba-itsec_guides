# README
## Impala
### guide
- [guide](Impala/Impala_guide.md)
- older guides and attempts/notes [here](Impala/Old)

### scripts
- [run_impala](Impala/Scripts/run_impala.sh)
- [kill_running](Impala/Scripts/Kill_running.sh)
- [pre-build](Impala/Scripts/pre-build.sh)
  - the pre-build script is work in progress

## Suricata
### guide
- [guide](suricata/Suricata.md)

### scripts
 - TODO